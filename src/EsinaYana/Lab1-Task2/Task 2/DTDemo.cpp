﻿#include "DT.h"
#include <iostream>
#include <string>
#include <time.h>
using namespace std;



int main()
{
	setlocale(LC_ALL, "rus");
	DateTime dt;
	dt.getToday();
	cout << endl;
	dt.getYesterday();
	cout << endl;
	dt.getTomorrow();
	cout << endl;
	dt.getFuture(10);
	cout << endl;
	dt.getFuture(5, 1, 1, 2001);
	cout << endl;
	dt.getPast(7);
	cout << endl;
	dt.getPast(10, 10, 10, 1990);
	cout << endl;
	dt.getWeekDay();
	cout << endl;
	dt.getMonth();
	cout << endl;
	dt.calcDifference(10, 8, 2017);
	cout << endl;
	dt.calcDifference(3, 3, 1992, 4, 3, 1999);
	DateTime dt1(3, 3, 2001);
	dt1.getToday();
	cout << endl;
	system("pause");
	return 0;
}