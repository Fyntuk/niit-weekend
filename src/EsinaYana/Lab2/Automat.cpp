﻿#include "Automat.h"
#include <iostream>
#include <fstream>
#include <string>

int Automata:: prices[] = { 10,15,20,25,25 };
int Automata::cash;
STATES Automata::state;
int Automata::position;
int Automata::surrender;
string Automata::message;

string Automata::coin(int sum)
{
	if (state != OFF)
	{
		if ((state == WAIT) || (state == ACCEPT))
		{
			state = ACCEPT;
			cash += sum;
			message = "Внесены деньги.\n";
			return message;
		}
		else if (state == CHECK)
		{
			message = "В состоянии CHECK нельзя вводить деньги.\n";
			return message;
		}
		else if (state == COOK)
		{
			message = "В состоянии COOK нельзя вводить деньги.\n";
			return message;
		}
	}
}
string Automata::getMenu() const
{
	if (state != OFF)
	{
		char buff[50]; // буфер промежуточного хранения считываемого из файла текста
		string menu = "";
		ifstream fin("Menu.txt");
		if (!fin.is_open()) // если файл не открыт
		{
			throw 1;//Файл не может быть открыт!
		}
		else
		{
			while (fin.getline(buff, 50))
			{
				menu += buff;
				menu += "\n";
			}
			fin.close(); // закрываем файл
			menu += buff;
			menu += "\n";
		}
		return menu;
	}
	else return message = "Автомат выключен.";
}
string Automata::getState() const
{
	if (state != OFF)
	{
		switch (state)
		{
		case 1: return "Состояние автомата - WAIT.\n";
		case 2: return "Состояние автомата - ACCEPT.\n";
		case 3: return "Состояние автомата - CHECK.\n";
		case 4: return "Состояние автомата - COOK.\n";
		}
	}
	else return message = "Автомат выключен.";
}
string  Automata::choise(int pos)
{
	if (state != OFF)
	{
		if (state == ACCEPT)
			if (check(pos))
			{
				position = pos - 1;
				surrender = cash - prices[position];
				message = "Выбран напиток.";
				state = CHECK;
				return message;
			}
			else
			{
				message = "Недостаточно средств на счете.\n";
				return message;
			}
		else if (state == CHECK)
		{
			message = "В состоянии CHECK нельзя выбрать напиток.\n";
			return message;
		}
		else if (state == COOK)
		{
			message = "В состоянии COOK нельзя выбрать напиток.\n";
			return message;
		}
		else if (state == WAIT)
		{
			message = "В состоянии WITE нельзя выбрать напиток.\n";
			return message;
		}
	}
	else return message = "Автомат выключен.";
}
bool Automata::check(int pos)
{
	if ((state == ACCEPT) || (state == CHECK))
		{
			if (cash >= prices[pos - 1])
			{
				state = CHECK;
				return true;
			}
			else
			{
				state = ACCEPT;
				return false;
			}
		}
		else return false;
}
string Automata::cancel()
{
	if (state != OFF)
	{
		if ((state == ACCEPT) || (state == CHECK))
		{
			if (cash != 0)
			{
				message = "Возврат сдачи.";
				cash = 0;
				surrender = 0;
			}
			state = WAIT;
			position = NULL;
		}
		return message;
	}
	else return message = "Автомат выключен.";
}
string Automata::cook()
{
	if (state != OFF)
	{
		if (state == CHECK)
		{
			if (check(position))
			{
				message = "Изготовление напитка.\nНапиток готов.\n";
				state = COOK;
				finish();
				return message;
			}
			else
			{
				message = "Недостаточно средств на счете";
				return message;
			}
		}
	}
	else return message = "Автомат выключен.";
}
void Automata::finish()
{
	if (state != OFF)
	{
		if (state == COOK)
		{
			cash -= prices[position];
			if (cash != 0)
			{
				message += "Возврат сдачи.\n";
				cash = 0;
				surrender = 0;
			}
			state = WAIT;
			position = NULL;
			message += "Обслуживание завершено.\n";
		}
	}
	else message = "Автомат выключен.";
}
