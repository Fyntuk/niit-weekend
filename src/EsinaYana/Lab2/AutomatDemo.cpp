﻿#include <iostream>
#include  "Automat.h"
#include <fstream>
#include <string>

using namespace std;

int main()
{
	setlocale(LC_ALL, "rus");
	Automata automat;
	try {
		cout << "Сценарий работы автомата по продаже горячих напитков." << endl << endl;
		cout << "Включим автомат." << endl;
		cout<<automat.on();
		cout << automat.getState() << endl;;
		cout << "Выведем меню на экран." << endl;		
		cout <<automat.getMenu()  << endl;
		cout << "Внесем деньги. 5 руб." << endl;
		cout<<automat.coin(5);
		cout << automat.getState();
		cout << "Попробуем выбрать напиток номер 1." << endl;
		cout << automat.choise(1);
		cout << "Средств на счете не достаточно. Внесем еще 5 руб. " << endl;
		cout<<automat.coin(5);
		cout << automat.getState();
		cout << "Закажем чай. Позиция 1." << endl;
		cout<<automat.choise(1);
		cout << automat.getState();
		cout << "Приготовим напиток." << endl;
		cout << automat.cook();
		cout << automat.getState();
		automat.cancel();
		automat.off();
	}
	catch(int a)
	{
		switch (a)
		{
		case 1: 
			cout << "Файл меню не может быть открыт." << endl;
		}
	};
	system("pause");
	return 0;
}