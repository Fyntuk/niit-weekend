#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <time.h>
#include <string>
#include "DateTime.h"

using namespace std;

int main()
{
	setlocale(LC_ALL, "rus");
	try
	{
		cout << "�������� ������ � ������� ��������"<<'\n'<< endl;
		DateTime dt;
		cout << dt.getToday() << endl;
		cout << dt.getYesterday() << endl;
		cout << dt.getTomorrow() << endl;
		cout << dt.getFuture(50) << endl;
		cout << dt.getPast(5) << '\n' << endl;
		cout << "� ������� ����������:" << '\n' << endl;
		cout << dt.getMonth() << endl;
		cout << dt.getWeekDay() << '\n' << endl;
		cout << "��������� ����������� �����������" << '\n' << endl;
		DateTime dt1 = dt;
		cout << dt1.getToday() << '\n' << endl;
		cout << "��������. � ������ ��������� ������ ������� � ���� -" << '\n' << endl;
		cout << "������� ���� " << dt1.calcDifference(dt) << '\n' << endl;
		cout << "������� ������� 0 ����. �������� � ����� �������" << '\n' << endl;
		cout << "�������� ��������� ���� ����" << '\n' << endl;
		//cout << "01.13.2015"  << endl;
		//DateTime dt3_1 (01, 13, 2015);						// ����������� ����� ������ ������, ��� ��� �������� � �������� 
		//cout << "�������� �����. ���������� 100.05.2015" << endl;
		//DateTime dt3_2 (100, 05, 2015);
		//cout << "����� ��������. ���������� 01.05.1969" << endl;
		//DateTime dt3_3 (01, 05, 1969);
		cout << "�������� �����. ���������� 01.05.2015" << endl;
		DateTime dt3(01, 05, 2015);
		cout << dt3.getToday() << endl;
		cout << "�������� ��������� ���� ���� ��� ���" << '\n' << endl;
		//DateTime dt4_1 (29, 2, 2017);
		cout << "���, ����� ������. ���������� 18.02.2017" << endl;
		DateTime dt4(18, 2, 2017);
		cout << dt4.getToday() << endl;
		cout << "������� ���� ����� 18.02.2017 � 01.05.2015, ����� " << dt4.calcDifference(dt3) << endl;
		cout << "� ���� �������� 01.05.2015 � 18.02.2017, ����� " << dt3.calcDifference(dt4) << endl;
		cout << "� ���������" << endl;
		//cout << dt3.calcDifference(dt3) << endl;
		cout << "����� ������������."  << endl;
	}
	catch (int error)
	{
		if (error == 1)
			cout << "���������� ���� � ������ ������� ���� ������ 31!" << endl;
		else if (error == 2)
			cout << "���������� ���� � ������ ������� ���� ������ 30!" << endl;
		else if (error == 3)
			cout << "���������� ���. ���������� ���� � ������ ������� ���� ������ 29!" << endl;
		else if (error == 4)
			cout << "�� ���������� ���. ���������� ���� � ������ ������� ���� ������ 28!" << endl;
		else if (error == 5)
			cout << "������� � ���� 12!" << endl;
		else if (error == 6)
			cout << "��� �� ����� ���� ������ 1970!" << endl;
		else if (error == 7)
			cout << "���� �� ����� ���� 0!" << endl;
		else if (error == 8)
			cout << "������������ ������� ���� ������ � ���� �� �������, �� ����� ������!" << endl;
		else
			cout << "Unknown error!" << endl;

	}

	return 0;
}

