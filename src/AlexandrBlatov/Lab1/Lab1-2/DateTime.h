#include <time.h>
#include <string>
#include <iostream>
#ifndef _DATETIME_H
#define _DATETIME_H
using namespace std;

class DateTime
{
private:
	static string WeekDays[7];  
	static string WeekMonth[12];
	static const int nullYear = 1900;
	int Day, Month, Year, WDay;
	time_t Seconds;
	
public:

	DateTime(int day, int month, int year, int h = 0, int m = 0, int s= 0) //����������� � ����� ��������� �����������(����, �����, ���);
	{
		DTEnter(year, day, month, h, m, s);
	};		
	
	DateTime()										//����������� ��� ����������(������ ���������� ������� ����);
	{				
		DTLocal();
	};									
	
	DateTime(const DateTime& dt) : Day(dt.Day), Month(dt.Month), // ����������� �����������(������ ����� ������� �������);
		Year(dt.Year), WDay(dt.WDay), Seconds(dt.Seconds) 
		{ };									
		
	string getToday()const;									//��� ������ �� ����� ������� ���� � ���� ������, � ��������� ��� ������ � �������� ������;
	string getYesterday()const;								//��� ������ �� ����� ���� ���������� ��� � ���� ������, � ��������� ��� ������ � �������� ������;
	string getTomorrow()const;								//��� ������ �� ����� ���� ����������� ��� � ���� ������, � ��������� ��� ������ � �������� ������.
	string getFuture(int n_day)const;							//��� ������ ���� ����� N ���� � �������;
//	int getPast(int d_day)const;							//��� ������ ���� ����� N ���� � �������;
	string getPast(int d_day)const;							//��� ������ ���� ����� N ���� � �������;
	string getMonth()const;									//��� ������ �������� �������� ������;
	string getWeekDay()const;								//��� ������ �������� �������� ��� ������;
	int calcDifference(const DateTime& dt);					// ��� ������� �������(� ����) ����� ����� ������
private:
	void DTEnter(int year, int day, int month, int h = 0, int m = 0, int s = 0);	//��� ������������� �������
	void DTLocal();																	//��� ���������� �������				
};


#endif
