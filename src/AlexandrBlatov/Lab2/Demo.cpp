#include "Automat.h"
#include <iostream>
#include <string>

using namespace std;

int main()
{
	setlocale(LC_ALL, "rus");
	try
	{
		Automat Fist;
		Fist.on();							//�������� �������
		cout << "1 " << Fist.getState() << endl;	//��������� ��������� ��������
		Fist.coin(5);						//������ ������
		cout << "2 " << Fist.getState() << endl;	//��������� ��������� ��������
		cout << "3 " << '\n'<< Fist.getMenu() << endl;			//������� ����
		Fist.choice(2);						//�������� �������									
		cout << "4 " << Fist.getState() << endl;	//��������� ��������� ��������
		//Fist.choice(0);						//�������� �������						//���������� 4			
		//cout << "4 " << Fist.getState() << endl;	//��������� ��������� ��������
		Fist.check();						//��������� ������� �� �����
		cout << "5 " << Fist.getState() << endl;	//��������� ��������� ��������
		Fist.cancel();						//������������ � ������
		cout << "6 " << Fist.getState() << endl;	//��������� ��������� ��������
		Fist.coin(20);						//����� ������ ������	
		cout << "7 " << Fist.getState() << endl;	//��������� ��������� ��������
		Fist.coin(15);						//��� ��� ������ ������
		cout << "8 " << Fist.getState() << endl;	//��������� ��������� ��������
		Fist.choice(2);						//�������� �������									
		cout << "9 " << Fist.getState() << endl;	//��������� ��������� ��������
		Fist.check();						//��������� ������� �� �����
		cout << "10 " << Fist.getState() << endl;	//��������� ��������� ��������
		//Fist.coin(5);						//������ ������							//���������� 1
		//cout << "2 " << Fist.getState() << endl;	//��������� ��������� ��������
		Fist.cook();						//������� �������
		cout << "11 " << Fist.getState() << endl;	//��������� ��������� ��������
		//Fist.coin(-5);						//������ ������						//���������� 3
		//cout << "2 " << Fist.getState() << endl;	//��������� ��������� ��������
		Fist.finish();						// �����
		cout << "12 " << Fist.getState() << endl;	//��������� ��������� ��������
		Fist.off();							//���������
		cout << "13 " << Fist.getState() << endl;	//��������� ��������� ��������
		//Fist.coin(5);						//������ ������							//���������� 2
		//cout << "2 " << Fist.getState() << endl;	//��������� ��������� ��������
	}
	catch (int error)
	{
		if (error == 1)
			cout << "������� � ������ ������������� �������, ���������� ����� �� ���� ����������!" << endl;
		else if (error == 2)
			cout << "������� ��������. ��� �������� ����� �� ���� ������� ���������� �������� �������!" << endl;
		else if (error == 3)
			cout << "���������� �������� ����� �� ����� ���� �������������!" << endl;
		else if (error == 4)
			cout << "������� �����������. ������� ���������� ����� �� 1 �� 7!" << endl;
		else
			cout << "Unknown error!" << endl;

	}
	
	return 0;
}
