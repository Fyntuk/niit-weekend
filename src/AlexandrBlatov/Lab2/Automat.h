#include <string>
#include <iostream>
#include <sstream>
enum STATES {OFF, WAIT, ACCEPT, CHECK, COOK};
using namespace std;

#ifndef _AUTOMAT_H
#define _AUTOMAT_H

class Automat
{
private:
	static string Menu[7];
	static string Price_str[7];
	static int Price[7];
	int Stat;			// ��������� �������� ���/����
	int Account;		// ���������� ����� �� �����
	string str_Account;
	int Choice;			// ����� ���������� �������
	int Change;         // �����
	string str_Change;
	bool FlagChoice;	// False - ����� ������� �� ������, True - ������
public:
	Automat() : Stat(OFF), Account(0), str_Account ("0"),
		Choice(0), Change(0), str_Change ("0"), FlagChoice(false) {};					// �����������
	void on() { Stat = WAIT; Account = 0; str_Account = "0";
				Choice = 0; Change = 0;  str_Change = "0"; FlagChoice = false;};			// ��������� ��������;
	void off(){ Stat = OFF; Account = 0; str_Account = "0"; 
				Choice = 0; Change = 0; str_Change = "0"; FlagChoice = false; };				//���������� ��������;
	void coin(int coin);		//��������� ����� �� ���� �������������;
	string getMenu() const;	//����������� ���� � ��������� � ������ ��� ������������;
	string getState() const;		//����������� �������� ��������� ��� ������������;
	void choice(int n);				//����� ������� �������������;
	void check();				//�������� ������� ����������� �����;
	void cancel();				//������ ������ ������������ �������������;
	void cook();				//�������� �������� ������������� �������;
	void finish();				//���������� ������������ ������������.
private:
	//inline string intTOstr(int a)
static	string intTOstr(int a)	// ����������� �������� int � string
	{
		ostringstream b;
		b << a;
		string b_str = b.str();							
		return b_str;
	}
};

#endif
