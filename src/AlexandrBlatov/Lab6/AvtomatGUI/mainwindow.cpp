#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QLCDNumber>
#include <QLabel>
#include <QProgressBar>
#include <QMessageBox>
#include <QString>
#include "Automat.h"
#include <string>
#include <sstream>
#include <QRegExp>
#include <QDebug>
#include <QTextCodec>
#include <QByteArray>

using namespace std;
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
Money = 0;
menu_size = 0;
qt=new QTimer;

tval=0;
//****************** строим кнопки*******************
  setWindowTitle(tr("Koffe avtomat"));
  art_windows();
//*****************Connect******************************
connect(b_ON_2,SIGNAL(clicked()),this,SLOT(start()));
connect(b_OFF_2,SIGNAL(clicked()),this,SLOT(stop()));
connect(b_Cancel_2,SIGNAL(clicked()),this,SLOT(cancel()));

for(int it = 0; it<menu_size-1; ++it)
  connect(b_menu[it],SIGNAL(clicked()),this,SLOT(menu()));

connect(b_Money_2,SIGNAL(clicked()),this,SLOT(coin()));
connect(b_Accept_2,SIGNAL(clicked()),this,SLOT(Accept()));
connect(b_Cook_1,SIGNAL(clicked()),this,SLOT(cook()));
connect(this->qt,SIGNAL(timeout()),this,SLOT(timeout()));
//******************************************************
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::start()
{
drink.on();

LCD_Account_2->display(0);
LCD_Change_6->display(0);
LCD_Money_2->display(0);
LCD_Price_2->display(0);
LCD_Change_6->display(0);
Label_Cook_1->setText("...............");

b_ON_2->setDisabled(true);
b_OFF_2->setDisabled(false);
b_Cancel_2->setDisabled(false);
b_Money_2->setDisabled(false);
b_Accept_2->setDisabled(false);
b_Change_6->setDisabled(true);

for(int it = 0; it<menu_size-1; ++it)
   {
    b_menu[it]->setDisabled(false);
    l_menu[it]->setStyleSheet("color: rgb(0,255,0)");
    l2_menu[it]->setStyleSheet("color: rgb(0,255,0)");
    }
}

void MainWindow::stop()
{
drink.off();
qt->stop();
Bar_Cook_1->setValue(0);
tval=0;

LCD_Account_2->display(0);
LCD_Change_6->display(0);
LCD_Money_2->display(0);
LCD_Price_2->display(0);
Label_Cook_1->setText("...............");

b_ON_2->setDisabled(false);
b_OFF_2->setDisabled(true);
b_Money_2->setDisabled(true);
b_Accept_2->setDisabled(true);
b_Cancel_2->setDisabled(true);

for(int it = 0; it<menu_size-1; ++it)
   {
    b_menu[it]->setDisabled(true);
    l_menu[it]->setStyleSheet("color: rgb(204,204,204)");
    l2_menu[it]->setStyleSheet("color: rgb(204,204,204)");
    }
}

void MainWindow::cancel()
{
 qt->stop();
 drink.cancel();
 string str = drink.getState();
//************************************************************
QString qstr(str.c_str());
QStringList list = qstr.split("\n");
int stat_size = list.size();
//*************************************************************
QRegExp re("(\\d+)");
if (stat_size !=0)
    re.indexIn(list[0],0);

LCD_Change_6->display(re.cap( 0 ));

LCD_Account_2->display(0);
LCD_Change_6->display(re.cap( 0 ));
LCD_Money_2->display(0);
LCD_Price_2->display(0);
Label_Cook_1->setText("...............");
Money=0;

if(re.cap( 0 )!=0 || LCD_Change_6->intValue() != 0)
   art_cancel();
}

void MainWindow::coin()
{
QTime midnight(0,0,0);
qsrand(midnight.secsTo(QTime::currentTime()));
Money = qrand()%30 ;
LCD_Money_2->display(Money);
}

void MainWindow::Accept()
{
try
  {
  drink.coin(Money);
  string str = drink.getState();
  Money = 0;
  LCD_Money_2->display(Money);
//************************************************************
  QString qstr(str.c_str());
  QStringList list = qstr.split("\n");
  int stat_size = list.size();
//*************************************************************
  QRegExp re(" (\\d+) ");
  if (stat_size ==1) //внос денег (напиток не выбран)
      re.indexIn(list[0],0);
  else if(stat_size !=0) //внос денег,  напиток выбран
     {
      re.indexIn(list[0],0);
      QRegExp re_2(" (\\d+) | (-\\d+) ");
      re_2.indexIn(list[1],0);
      bool ok;
      int x = re_2.cap(0).toInt(&ok,10);

      if ( x>0 && ok == true)  //денег недостаточно
         {
          QString addMoney = "Добавьте - ";
          addMoney +=  re_2.cap(0) + " руб.";
          Label_Cook_1->setText(addMoney);
         }
      else                   //денег достаточно, считаем сдачу
         {
          QStringList listWord = re_2.cap(0).split("-");
          QString add = listWord[1];
          Label_Cook_1->setText(temp);
          LCD_Change_6->display(add);
          b_Cook_1->setDisabled(false);
         }
      }
  LCD_Account_2->display(re.cap( 0 ));
  }
catch (int error)
  {
   QMessageBox qmbError;
   if (error == 1)
      {
       qmbError.setText("Автомат в режиме приготовления напитка, добавление денег на счет невозможно!");
       qmbError.exec();
       endError();
      }
   else if (error == 2)
      {
       qmbError.setText("Автомат выключен. Для внесения денег на счет сначала необходимо включить автомат!");
       qmbError.exec();
       endError();
      }
   else if (error == 3)
      {
       qmbError.setText("Количество вносимых денег не может быть отрицательным!");
       qmbError.exec();
       endError();
      }
   else
      {
       qmbError.setText("Unknown error!");
       qmbError.exec();
       endError();
      }
  }
}

void MainWindow::menu()
{
try
   {
    for(int it = 0; it<menu_size-1; ++it)
      {
       if (b_menu[it] !=((QPushButton*)sender()))
          {
           b_menu[it]->setDisabled(true);
           l_menu[it]->setStyleSheet("color: rgb(204,204,204)");
           l2_menu[it]->setStyleSheet("color: rgb(204,204,204)");
          }
       else
          {
           b_menu[it]->setDisabled(false);
           l_menu[it]->setStyleSheet("color: rgb(0,255,0)");
           l2_menu[it]->setStyleSheet("color: rgb(0,255,0)");
           Label_Cook_1->setText(l_menu[it]->text());
           temp = l_menu[it]->text();
           LCD_Price_2->display(l2_menu[it]->text());
           drink.choice(it+1);
          }
      }
    if(LCD_Price_2->intValue() <= LCD_Account_2->intValue())
      {
       b_Cook_1->setDisabled(false);
       LCD_Change_6->display((LCD_Account_2->intValue())-(LCD_Price_2->intValue()));
      }
    else
       b_Cook_1->setDisabled(true);
  }
catch (int error)
  {
   QMessageBox qmbError;
   if (error == 4)
        {
         qmbError.setText("Напиток отсутствует. Введите пожалуйста цифру от 1 до 7!");
         qmbError.exec();
         endError();
        }
    else
        {
         qmbError.setText("Unknown error!");
         qmbError.exec();
         endError();
        }
  }
}

void MainWindow::cook()
{
drink.check();
string str = drink.getState();
QString qstr(str.c_str());
QStringList list = qstr.split("\n");
  if (list[0].at(0) == "А")
     {
      drink.cook();
      tval=0;
      qt->setInterval(50);
      qt->start();
     }
}

void MainWindow::timeout()
{
if(tval<=100)
   Bar_Cook_1->setValue(tval++);
else
  {
   qt->stop();
   Label_Cook_1->setText("Напиток готов!!!");
   b_Cook_1->setDisabled(true);
   art_cancel();
  }
}

void MainWindow::art_cancel()
{
if(LCD_Change_6->intValue()!=0)
    b_Change_6->setDisabled(false);

b_OFF_2->setDisabled(true);
b_Money_2->setDisabled(true);
b_Accept_2->setDisabled(true);
b_Cancel_2->setDisabled(true);

LCD_Account_2->display(0);
LCD_Price_2->display(0);

for(int it = 0; it<menu_size-1; ++it)
   {
    b_menu[it]->setDisabled(true);
    l_menu[it]->setStyleSheet("color: rgb(204,204,204)");
    l2_menu[it]->setStyleSheet("color: rgb(204,204,204)");
   }
Bar_Cook_1->setValue(0);
tval=0;
connect(b_Change_6,SIGNAL(clicked()),this,SLOT(start()));
}

void MainWindow::art_windows()
{
layout->addLayout(layout1,0,0,1,4);   //координаты левого верхнего угла (строка, столбец), высота строки, ширина столбца
layout->addLayout(layout2,1, 0, 2, 4);
layout2->addLayout(layout2_1);
layout2->addLayout(layout2_2);
layout2->addLayout(layout2_3);
layout2->addLayout(layout2_4);
layout->addLayout(layout3,3,0, 7 ,1);
layout->addLayout(layout4,3,1,7,3);
layout->addLayout(layout5,3,3,7,1);
layout->addLayout(layout6,10,2,1,2);

Label_Cook_1->setFrameStyle(QFrame::Panel | QFrame::Sunken);
Label_Cook_1->setAlignment(Qt::AlignVCenter | Qt::AlignLeft);
Label_Cook_1->setMinimumWidth(125);

Bar_Cook_1->setValue(0);

Label_Price_2->setFrameStyle(QFrame::Panel | QFrame::Sunken);
Label_Price_2->setAlignment(Qt::AlignVCenter | Qt::AlignLeft);
Label_Account_2->setFrameStyle(QFrame::Panel | QFrame::Sunken);
Label_Account_2->setAlignment(Qt::AlignVCenter | Qt::AlignLeft);

Label_Change_6->setFrameStyle(QFrame::Panel | QFrame::Sunken);
Label_Change_6->setAlignment(Qt::AlignVCenter | Qt::AlignLeft);

LCD_Price_2->display(0);
LCD_Account_2->display(0);
LCD_Money_2->display(0);
LCD_Change_6->display(0);

b_OFF_2->setDisabled(true);
b_Cook_1->setDisabled(true);
b_Money_2->setDisabled(true);
b_Accept_2->setDisabled(true);
b_Cancel_2->setDisabled(true);

string str = drink.getMenu();
QString qstr(str.c_str());
QStringList list = qstr.split("\n");
menu_size = list.size();

for(int i=0;i<menu_size-1;++i )
   {
    QString b_name= "Напиток " + QString("%1").arg(i+1);
    b_menu.push_back(new QPushButton(b_name));
    QStringList listWord = list[i].split(" - ");
    QString menu_1 = listWord[0];
    QString menu_2 = listWord[1];
    l_menu.push_back(new QLabel(menu_1));
    l2_menu.push_back(new QLabel(menu_2));
   }

for(int it = 0; it<menu_size-1; ++it)
   {
    layout3->addWidget(b_menu[it]);
    b_menu[it]->setDisabled(true);

    layout4->addWidget(l_menu[it]);
    l_menu[it]->setFrameStyle(QFrame::Panel | QFrame::Sunken);
    l_menu[it]->setAlignment(Qt::AlignVCenter | Qt::AlignLeft);
    l_menu[it]->setStyleSheet("color: rgb(204,204,204)");

    layout5->addWidget(l2_menu[it]);
    l2_menu[it]->setFrameStyle(QFrame::Panel | QFrame::Sunken);
    l2_menu[it]->setAlignment(Qt::AlignCenter | Qt::AlignLeft);
    l2_menu[it]->setStyleSheet("color: rgb(204,204,204)");
   }

b_Change_6->setDisabled(true);

layout1->addWidget(b_Cook_1);
layout1->addWidget(Label_Cook_1);
layout1->addWidget(Bar_Cook_1);

layout2_1->addWidget(Label_Price_2);
layout2_1->addWidget(Label_Account_2);

layout2_2->addWidget(LCD_Price_2);
layout2_2->addWidget(LCD_Account_2);

layout2_3->addWidget(LCD_Money_2);
layout2_3->addWidget(b_Money_2);
layout2_3->addWidget(b_Accept_2);

layout2_4->addWidget(b_ON_2);
layout2_4->addWidget(b_OFF_2);
layout2_4->addWidget(b_Cancel_2);

layout6->addWidget(b_Change_6);
layout6->addWidget(Label_Change_6);
layout6->addWidget(LCD_Change_6);

ui->centralWidget->setLayout(layout);
}

void MainWindow::endError()
{
 QMessageBox qmb;
 qmb.setText("Возвращаемся назад после ошибки.");
 qmb.exec();
}

