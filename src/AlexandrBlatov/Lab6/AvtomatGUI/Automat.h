﻿#include <string>
#include <iostream>
#include <sstream>
//#include <QObject>

using namespace std;

#ifndef _AUTOMAT_H
#define _AUTOMAT_H

class Automat
{
private:
	static string Menu[7];
	static string Price_str[7];
	static int Price[7];
    int Stat;			// состояние автомата Вкл/Выкл
    // OFF=1, WAIT=2, ACCEPT=3, CHECK=4, COOK=5
    int Account;		// количество денег на счету
	string str_Account;
	int Choice;			// номер выбранного напитка
	int Change;         // сдача
	string str_Change;
	bool FlagChoice;	// False - выбор напитка не сделан, True - сделан
public:
    Automat() : Stat(1),Account(0), str_Account ("0"), Choice(0), Change(0), str_Change ("0"), FlagChoice(false) {}
    void on() { Stat = 2; Account = 0; str_Account = "0"; Choice = 0; Change = 0;  str_Change = "0"; FlagChoice = false;}
    void off(){ Stat = 1; Account = 0; str_Account = "0"; Choice = 0; Change = 0; str_Change = "0"; FlagChoice = false; }
    void coin(int coin);		//занесение денег на счёт пользователем;
	string getMenu() const;	//отображение меню с напитками и ценами для пользователя;
	string getState() const;		//отображение текущего состояния для пользователя;
	void choice(int n);				//выбор напитка пользователем;
	void check();				//проверка наличия необходимой суммы;
	void cancel();				//отмена сеанса обслуживания пользователем;
	void cook();				//имитация процесса приготовления напитка;
	void finish();				//завершение обслуживания пользователя.
private:

static	string intTOstr(int a)	// Преобразуем значение int в string
	{
		ostringstream b;
		b << a;
		string b_str = b.str();							
		return b_str;
	}
};

#endif
