#include "Dekanat.h"
#include <string>
#include <iostream>
#include <vector>
#include <iterator>
#include <fstream>
#include <sstream>
#include <algorithm>


int Student::TotalID = 0;
Dekanat* Dekanat::Singl = NULL;
int Dekanat::Count = 0;
//************************************************************************************
static bool YesNo(string str)
{
	cout << str << ": Y/N ";
	string answer;
	do
	{
		cin >> answer;
		if (answer == "Y" || answer == "y" || answer == "Yes" || answer == "YES" || answer == "�" || answer == "�" || answer == "��" || answer == "��")
			return true;
		else if (answer == "N" || answer == "n" || answer == "No" || answer == "NO" || answer == "�" || answer == "�" || answer == "���" || answer == "���")
			return false;
		else
		{
			cout << "������� ����� � �������(�/�; �/�; ��/���; Y/N; y/n; Yes/No)";
		}
	} while (1);
}
static string IntToString(int x)
{
	ostringstream result;
	result << x;
	string str_result = result.str();
	return str_result;
}
static void Out(string str)
{
	cout << str;
}
static string In(void)
{
	string str;
	cin >> str;
	return str;

}

static string FirstWord(string longstr)
{
	string f = longstr;
	char str[100];
	strcpy_s(str, f.c_str());
	char seps[] = " ,\t\n";
	char* token1 = NULL;
	char* next_token1 = NULL;
	token1 = strtok_s(str, seps, &next_token1);
	string result = token1;
	return result;
}

//************************************************************************************
//**************************������� ������� ������ 1 ���******************************
Dekanat* Dekanat::Create()
{
	if (!Singl)
		Singl = new Dekanat;
	return Singl;
}
void Dekanat::delDekanat() //������� �������
{
	Count--;
	if (!Count)
	{
		delete this;
		Singl = NULL;
	}
}
//**************************************************************************************
//**************************************************************************************
void Dekanat::setFileRead()		//�������� ����� � ��������� �� ������ ������ �� �����
{
	ifstream fgroup("DATA.txt");
	if (fgroup.fail())
		cerr << "File DATA.txt error" << endl;

	string NumGroup;
	while (1)
	{
		fgroup >> NumGroup;
		if (fgroup.eof())
			break;
		Group *group;
		group = new Group(NumGroup);
		this->Groups.push_back(group);
		setFileRead(NumGroup);	// ������ ���� �� ���������� �������������� ������ ������
		string fhead;
		fgroup >> fhead;
		if (fhead != "-")
			group->appHead(fhead);
	}
	fgroup.close();
}

void Dekanat::setFileRead(string groupName)		//�������� ��������� �� ������ ������ �� �����
{
	string fileName = groupName + ".txt";
	ifstream fstuds(fileName);
	if (fstuds.fail())
		cerr << "File error" << endl;

	string id, surname, name, midname, fio;
	Group *ptrGroup = searchGroup(groupName);

	while (1)
	{
		fstuds >>id>> surname >> name >> midname;
		if (fstuds.eof())
			break;
		fio = surname + " " + name + " " + midname;
		Student* stud;
		stud = new Student(fio);
		
		stud->ID = atoi(id.c_str());
		stud->setGroup(ptrGroup);
		ptrGroup->Students.push_back(stud);
		++ptrGroup->Num;

		string str_marks;
		getline(fstuds, str_marks);
		int count = str_marks.length();
		for (int i = 0; i < count; i++)
		{
			if (str_marks.at(i) < '6' && str_marks.at(i) > '0')
			stud->setMarks(str_marks.at(i) - 48);
		}
	}
	fstuds.close();
}

void Dekanat::getFileWrite()const	//���������� ����������� ������ � ������
{
	string fNameGroup = "DATA.txt"; //��������� ���� � ������������� �����
	ofstream fout;
	fout.open(fNameGroup, ios::out);
	for (unsigned int i = 0; i < Groups.size(); i++)
	{
		fout << Groups[i]->Title<< " ";
		if (Groups[i]->Head == nullptr)
			fout << "- \n";
		else
			fout << FirstWord(Groups[i]->Head->Fio) << " \n"; 
	}
	fout.close();
	Out("DATA.txt - ��������. \n");

	for (unsigned int i = 0; i < Groups.size(); i++) // ��������� ����� ����� �� ����������
	{
		string fileName;
		Group* ptrGroup = Groups[i];
		fileName = ptrGroup->Title +".txt";
		ofstream fout;
		fout.open(fileName, ios::out); //���� ����������� ��� ���� � ����� ������������
		for (unsigned int j = 0; j < ptrGroup->Students.size(); j++)
		{
			fout << ptrGroup->Students[j]->ID << " ";	// ����� � ���� ID
			fout << ptrGroup->Students[j]->Fio << " ";	// ����� � ���� ���

			for (int k = 0; k < ptrGroup->Students[j]->Num; k++)// ����� � ���� ������ ������
			{
				fout << ptrGroup->Students[j]->Marks[k] << " ";
			}
			fout << '\n';
		}
	fout.close();
	string report = fileName +" -��������. \n";
	Out(report);
	}
}

Group* Dekanat::searchGroup(string groupName) const //����� ������
{
	for (int i = 0; i != Groups.size(); ++i)
	{
		string f = Groups[i]->Title;
		if (strcmp(groupName.c_str(), f.c_str()) == 0)
			return Groups[i];
	}
	return nullptr;
}
Student* Dekanat::searchStud(Group* ptrGroup) const ////����� �������� � ������
{
	Student* ptrStud;
	string str;
	str = "����� �������� �� ID (��), �� ���(���)";
	if (YesNo(str) == true)
	{
		int id;
		Out("������� ID ��������: ");
		id = atoi(In().c_str());
		ptrStud = ptrGroup->searchStud(id);
	}
	else
	{
		string fio;
		Out("������� ��� ��������: ");
		fio = In();
		ptrStud = ptrGroup->searchStud(fio);
	}
	return ptrStud;
}
void Dekanat::rndMarks(Student &stud) const//���������� ��������� ������ ���������
{
	int mark = 0;
	mark = rand()%5+1;
	string str = "������� " + stud.Fio + " ������� ������ - " + IntToString(mark) + ".\n";
	str += "����������� ���������� ������";
	if (YesNo(str) == true)
	stud.setMarks(mark);
}
void Dekanat::ExpellStudent()const //���������� ��������� �� ��������������
{
	string null = "EXPELLED";
	Group *nullgroup = nullptr;
	for (unsigned i = 0; i < this->Groups.size(); ++i) // ������� ������ ��� "�����������"
	{
		if (strcmp(Groups[i]->Title.c_str(), null.c_str()) == 0)
			nullgroup = Groups[i];
	}

	for (unsigned i = 0; i < this->Groups.size(); ++i)
	{
		Group* ptrGroup = Groups[i];

		if (strcmp(ptrGroup->Title.c_str(), null.c_str()) != 0) //������ != ������ "EXPELLED"
		{
			for (unsigned j = 0; j < ptrGroup->Students.size(); ++j) //������������� ���� ���������
			{
				Student* ptrStud = ptrGroup->Students[j];
				if (ptrStud->getAvMarkStud() < 3)				//������� ���� �������� ������ 3
				{
					string str;
					str = "����������� ������ - " + ptrGroup->Title + ".\n";
					str += "������� ID - " + IntToString(ptrStud->ID) + ", ��� - " + ptrStud->Fio + ".\n";
					str += "����� ������� ���� - " + IntToString(ptrStud->getAvMarkStud()) + ".\n";
					str += "����������� ���������� ��������";
					if (YesNo(str) == true)   
					{
						transferStudent(*ptrStud, *ptrGroup, *nullgroup);
					}
				}
			}
		}
	}
}

void Dekanat::transferStudent(Student &stud, Group &oldgroup, Group &newgroup) const
{
	
	Student* oldHead = oldgroup.Head;
	oldgroup.delStudToGroup(stud);
	
	if (stud.group == nullptr)
			newgroup.addStudent(stud);
}

void Dekanat::electHeads() const // ����� ������� �����
{
	for (unsigned i = 0; i < Groups.size(); ++i)
	{
		Group* ptrGroup = Groups[i];
		if (ptrGroup->Title != "EXPELLED") //��������� ������� �� ���� ������� ����� ������ "EXPELLED"
		{
			Out(ptrGroup->getlistGroup());
			if (ptrGroup->Num == 0) //� ������ ��� ��������� ��������� � ���������
			{
				Out("� ������ ��� ����������� ���������, ����� �������� ����������");
				Student* ptrStud = nullptr;
			}
			else if (Groups[i]->Head != nullptr) //� ������ ��� ���� ��������
			{
				string str = "� ������ ��� ���� ��������. " + Groups[i]->Head->Fio + ". �������� ������?";
				if (YesNo(str) == true)
					Groups[i]->Head = nullptr;
			}
			else if (Groups[i]->Head == nullptr)
			{
				string str = "����� �������� �� ID (��), �� ���(���)";
				if (YesNo(str) == true)
				{
					do
					{
						int id;
						Out("������� ID ��������: ");
						id = atoi(In().c_str());
						ptrGroup->appHead(id);
					} while (ptrGroup->Head == nullptr);
				}
				else
				{
					do
					{
						string fio;
						Out("������� ��� ��������: ");
						fio = In();
						ptrGroup->appHead(fio);
					} while (ptrGroup->Head == nullptr);
				}
				Student* ptrStud = ptrGroup->Head;
			}
		}
	}
}
string Dekanat::getStat(STATISTICS Stat) const//���������� ���������� �� ������������ ��������� � �����
{
	string str;
	Group *ptrGroup;
	Student* ptrStud;
	int groupNum;
	char num;
	switch (Stat)
	{
	case STAT_AV_STUDENT: //������� ������ �� ��������
		Out("�������� ������ �� ������: \n");
		Out(getConsole(LIST_NUM_GROUP));
		num = In()[0];
		groupNum = num - 48;
		ptrGroup = Groups.at(groupNum);
		str = ptrGroup->getlistGroup();
		Out(str);
		ptrStud = searchStud(ptrGroup);
		str = "������� ���� �������� " + ptrStud->Fio + " ��������� - " + IntToString(ptrStud->getAvMarkStud());
		return str;
		break;
	
	case STAT_AV_GROUP: //������� ������������ �� ������
		Out("�������� ������ �� ������: \n");
		Out(getConsole(LIST_NUM_GROUP));
		num = In()[0];
		groupNum = num - 48;
		ptrGroup = Groups.at(groupNum);
		str = "������� ���� �� ������ " + ptrGroup->Title + "��������� - " + IntToString(ptrGroup->getAvMarkGroup());
		
		return str;
		break;
	}
	return 0;
}
string Dekanat::getConsole(CONSOLE Console) const//����� �� �������
{
	string str ="";
	Group* ptrGroup;
	Student* ptrStud;	
	switch (Console)
	{
	case LIST_NUM_GROUP:
		
		for (unsigned i = 0; i < Groups.size(); ++i) // ������ �����
		{
			if (Groups[i]->Title != "EXPELLED")
			str += IntToString(i)+". " + Groups[i]->Title + "\n";
		}
		return str;
		break;

	case LIST_GROUP: //������ ��������� ������ � ��������� ID � ���
		int groupNum;
		Out("�������� ������ �� ������: \n");
		Out(getConsole(LIST_NUM_GROUP));
		char num;
		num	= In()[0];
		groupNum = num-48;
		str = Groups[groupNum]->getlistGroup();
		return str;
		break;

	case ADD_MARK_STUDENT: //���������� ������ ��������
		Out("�������� ������ �� ������: \n");
		Out(getConsole(LIST_NUM_GROUP));
		num = In()[0];
		groupNum = num - 48;
		ptrGroup = Groups.at(groupNum);
		str = ptrGroup->getlistGroup();
		Out(str);
		ptrStud = searchStud(ptrGroup);
		
		do
		{
			rndMarks(*ptrStud);
		} while (YesNo("���������� ���������� ������"));
		
		str = "������� - " + ptrStud->Fio + " ����� ��������� ������: \n";
		for (int i = 0; i < ptrStud->Num; ++i)
			str += IntToString(ptrStud->Marks[i]) + " ";
		return str;
		break;

	case LIST_STUDENT: //������ ������ �������� � ��� � ID
		Out("�������� ������ �� ������: \n");
		Out(getConsole(LIST_NUM_GROUP));
		num = In()[0];
		groupNum = num - 48;
		ptrGroup = Groups.at(groupNum);
		str = ptrGroup->getlistGroup();
		Out(str);
		ptrStud = searchStud(ptrGroup);
		str = "������� - " + ptrStud->Fio + " ����� ��������� ������: \n";
			for (int i = 0; i < ptrStud->Num; ++i)
				str += IntToString(ptrStud->Marks[i]) + " ";
		return str;
		break;

	case STUD_TRANSFER: //������� ���������
		Out("�������� ������ �� ������: \n");
		Out(getConsole(LIST_NUM_GROUP));
		num = In()[0];
		groupNum = num - 48;
		Group* ptrOldGroup;
		Group* ptrNewGroup;
		ptrOldGroup = Groups.at(groupNum);
		str = ptrOldGroup->getlistGroup();
		Out(str);
		ptrStud = searchStud(ptrOldGroup);
		Out("�������� ������ ���� ����� ��������� ������� �� ������: \n");
		Out(getConsole(LIST_NUM_GROUP));
		num = In()[0];
		groupNum = num - 48;
		ptrNewGroup = Groups.at(groupNum);
		transferStudent(*ptrStud, *ptrOldGroup, *ptrNewGroup);
		str = ptrNewGroup->getlistGroup();
		Out(str);
		return "������� �������� �������� �������."; 
		break;
	case STUD_EXPELLED: //���������� ��������� �� ��������������
		ExpellStudent();
		return "All EXPELLED!!!";
		break;
	case LIST_EXPELLED: //������ ����������
		ptrGroup = searchGroup("EXPELLED");
		str = ptrGroup->getlistGroup();
		return str;
		break;
	}
	return 0;
}


//*****************************************************************************************
//***************************|| class Group ||*********************************************
//*****************************************************************************************

void Group::setGroup(string title)//; //��������/�������������� ������ � ��������� ��������
{
	Title = title;
}

string Group::getTitle()const //������ ������ ������
{
		return Title;
}


string Group::getHead() const			//	������ ��� ��������
{
	if (Head != nullptr)
		return Head->Fio; //return Head->getFio();
	else
		return NULL;
}

int Group::getAvMarkGroup() const	//	���������� �������� ����� � ������
{
	if (Num == 0)
		return 0;
	float summ = 0.0;
	int	i = 0;
	int	average = 0;
	for (i = 0; i < Num; ++i)
	{
		summ += Students.at(i)->getAvMarkStud();
	}
	average = int(summ / Num + 0.5);
	return average;
}

string Group::getlistGroup() const //������ ������ ��������� ������
{
	string str = "������ ��������� ������:";
	str = str + Title + '\n';

	for (unsigned i = 0; i < this->Students.size(); ++i)
	{
		string s;
		if (this->Students.at(i) == Head) //�������� ��������, ���� �� ���� 
			s = "* ";
		s += "ID - " + IntToString(this->Students.at(i)->ID);
		s += ", ��� - " + this->Students.at(i)->Fio;
		str += s;
		str += '\n';
	}
	return str;
}

void Group::appHead(string fio)			//	�������� �������� �� ���
{
	Student* ptrStud = searchStud(fio);
	if (ptrStud != nullptr)		//�������� �� �������������� �������� ������
		Head = ptrStud;
	else
	{
		Out("������� �� �������� � ������ ������!!! ��������� ���� ������. \n");
	}
}

void Group::appHead(int id)			//	�������� �������� �� ID
{
	Student* ptrStud = searchStud(id);
	if (ptrStud != nullptr) //�������� �� �������������� �������� ������
		Head = ptrStud;
	else
	{
		Out("������� �� �������� � ������ ������!!! ��������� ���� ������. \n");
	}
}

void Group::addStudent(Student &stud)		//	���������� ��������
{
	if (stud.group == nullptr)  //�������� �� ���������� ������ � ��������
	{
		stud.setGroup(this);
		Students.push_back(&stud);
		++Num;
	}
}

Student* Group::searchStud(string fio)	//	����� �������� �� ��� ��� ��
{
	for (int i = 0; i != Num; ++i)
	{
		string tok = FirstWord(Students.at(i)->Fio);
		if (strcmp(fio.c_str(), tok.c_str()) == 0)
			return Students.at(i);
	}
	return nullptr;
}

Student* Group::searchStud(int id)//	����� �������� �� ��� ��� ��
{
	for (int i = 0; i != Num; ++i)
	{
		if (id == Students.at(i)->ID)
			return Students.at(i);
	}
	return nullptr;
}

void Group::delStudToGroup(Student &stud)			//	���������� �������� �� ������
{
	if (Head == &stud)
	{
		string str = "������� "+ stud.Fio +" �������� ��������� ������ " + Title+ "\n";
		str += " ����������� ���������� ��������:";
		if (YesNo(str) == true)
		{
			for (unsigned i = 0; i < Students.size(); ++i)
			{
				if (Students[i] == &stud)
					Students.erase(Students.begin() + i);
			}
			Head = nullptr;
			stud.setGroup(nullptr);
			Num--;
		}
	}
	else
	{
		for (unsigned i = 0; i < Students.size(); ++i)
		{
			if (Students[i] == &stud)
				Students.erase(Students.begin() + i);
		}
		stud.setGroup(nullptr);
		Num--;
	}

}

//*****************************************************************************************
//***************************|| class Student ||*******************************************
//*****************************************************************************************

void Student::setStudent(string	fio = "0")  //�������� �������� � ��������� �� � ���
{
	Fio = fio;
}
void Student::setGroup(Group *group = nullptr) //	���������� � ������ (��������� ������)
{
	this->group = group;
}

void Student::setMarks(int *marks, int num)//	���������� ������� ������
{
	Num = num;

	if (Num != 0)
		Marks = new int[Num];

	int i = 0;
	while (i != Num)
	{
		Marks[i] = marks[i];
		++i;
	}
}

void Student::setMarks(int mark)//	���������� ������
{
	int* temp;
	int num=0;
	num = Num + 1;
	temp = new int[num];

	int i = 0;
	while (i != Num)
	{
		temp[i] = Marks[i];
		++i;
	}
	
	temp[num - 1] = mark;
	delete[] Marks;
	Marks = new int[num];
	Num = num;

	i = 0;
	while (i != num)
	{
		Marks[i] = temp[i];
		++i;
	}
}
int Student::getAvMarkStud() const	//	���������� ������� ������
{
	if (Num == 0)
		return 0;

	float summ = 0.0;
	int	i = 0;
	int	average = 0;
	for (i = 0; i < Num; ++i)
	{
		summ += Marks[i];
	}
	average = int(summ / Num + 0.5);
	return average;
}
