#include "Counter.h"
#include <iostream> 
using namespace std;

int main()
{
    Counter passengers;

    for(;
        passengers.get()<=100;
        passengers.inc())
        cout<<passengers.get()<<endl;

    while(passengers.get())
    {
        cout<<passengers.get()<<endl;
        passengers.dec();
    }

    return 0;
}