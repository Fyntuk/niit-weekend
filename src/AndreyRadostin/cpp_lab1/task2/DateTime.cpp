#include"DateTime.h"
#include<iostream>
#include<ctime>
#include<string>

using namespace std;

string WeekDays[7] = { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };
string Months[12] = { "January", "February", "March", "April", "May", "June", "July","August","September","October","November","December" };

void DateTime::getToday() const{
	struct tm *now;
	now = localtime(&sec);
	cout << now->tm_mday << " " << Months[now->tm_mon] << " " << now->tm_year+1900 << endl;
	cout << WeekDays[now->tm_wday] << " " << endl;
};

void DateTime::getYesterday() const{
	time_t sec1 = sec;
	struct tm * timeinfo;
	sec1 -= 24 * 60 * 60;
	timeinfo = localtime(&sec1);
	cout << timeinfo->tm_mday << " " << Months[timeinfo->tm_mon] << " " << timeinfo->tm_year + 1900 << endl;
	cout << WeekDays[timeinfo->tm_wday] << " " << endl;
};
void DateTime::getTommorow() const{
	time_t sec1 = sec;
	struct tm * timeinfo;
	sec1 += 24 * 60 * 60;
	timeinfo = localtime(&sec1);
	cout << timeinfo->tm_mday << " " << Months[timeinfo->tm_mon] << " " << timeinfo->tm_year + 1900 << endl;
	cout << WeekDays[timeinfo->tm_wday] << " " << endl;
};

void DateTime::getPast(int N_d) const{
	time_t sec1 = sec;
	struct tm * timeinfo;
	sec1 -= N_d*24 * 60 * 60;
	timeinfo = localtime(&sec1);
	cout << timeinfo->tm_mday << " " << Months[timeinfo->tm_mon] << " " << timeinfo->tm_year + 1900 << endl;
	cout << WeekDays[timeinfo->tm_wday] << " " << endl;
};

void DateTime::getFuture(int N_d) const{
	time_t sec1 = sec;
	struct tm * timeinfo;
	sec1 += N_d * 24 * 60 * 60;
	timeinfo = localtime(&sec1);
	cout << timeinfo->tm_mday << " " << Months[timeinfo->tm_mon] << " " << timeinfo->tm_year + 1900 << endl;
	cout << WeekDays[timeinfo->tm_wday] << " " << endl;
};

void DateTime::getMonth() const{
	struct tm *now;
	now = localtime(&sec);

	cout <<"Current month is " << Months[now->tm_mon] <<"."<< endl;
};

void DateTime::getWeekDay() const{
	struct tm *now;
	now = localtime(&sec);

	cout << "Today is " << WeekDays[now->tm_wday] << "." << endl;
};

time_t DateTime::getsec() const{
	return sec;
};


void DateTime::calcDifference(DateTime& ref) {
	time_t sec1=ref.getsec();
	time_t diff = ((sec > sec1) ? sec - sec1 : sec1 - sec) / (24*60*60);
	cout << "Difference is " << diff << " days." << endl;
}